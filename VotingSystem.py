def voting_system():
    print("Welcome to the Voting System")

   
    candidates = ["Candidate A", "Candidate B", "Candidate C"]
    votes = [0, 0, 0]

    while True:
        print("\nCandidates:")
        for i, candidate in enumerate(candidates):
            print(f"{i + 1}. {candidate}")

        try:
           
            vote = int(input("Enter the number of your chosen candidate (0 to exit): "))

           
            if vote == 0:
                break

           
            if 1 <= vote <= len(candidates):
               
                votes[vote - 1] += 1
                print("Vote recorded!")
            else:
                print("Invalid choice. Please enter a valid candidate number.")

        except ValueError:
            print("Invalid input. Please enter a number.")

    
    print("\nVoting Results:")
    for i, candidate in enumerate(candidates):
        print(f"{candidate}: {votes[i]} votes")

   
    winner_index = votes.index(max(votes))
    winner = candidates[winner_index]
    print(f"\n{winner} is the winner with {max(votes)} votes!")

if __name__ == "__main__":
    voting_system()

